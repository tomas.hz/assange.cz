import flask
import functools


def login_required(function):
	@functools.wraps(function)
	def wrapped_view(**kwargs):
		auth = flask.request.authorization

		if (
			auth is None
			or auth.username != flask.current_app.config["SIGNATURE_VIEW_USERNAME"]
			or auth.password != flask.current_app.config["SIGNATURE_VIEW_PASSWORD"]
		):
			return (
				"Unauthorized",
				401,
				{
					"WWW-Authenticate": "Basic realm=\"Login Required\""
				}
			)

		return function(**kwargs)

	return wrapped_view
