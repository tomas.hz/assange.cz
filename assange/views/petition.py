import flask
import flask_wtf
import requests
import sqlalchemy
import wtforms

import werkzeug.exceptions

from .. import database, utils


petition_blueprint = flask.Blueprint(
	"petition",
	__name__
)


class SignatureForm(flask_wtf.FlaskForm):
	captcha_solution = wtforms.HiddenField(
		name="frc-captcha-solution"
	)
	
	name = wtforms.StringField(
		validators=[
			wtforms.validators.InputRequired(),
			wtforms.validators.Length(max=64)
		]
	)
	surname = wtforms.StringField(
		validators=[
			wtforms.validators.InputRequired(),
			wtforms.validators.Length(max=64)
		]
	)
	
	street = wtforms.StringField(
		validators=[
			wtforms.validators.InputRequired(),
			wtforms.validators.Length(max=128)
		]
	)
	city = wtforms.StringField(
		validators=[
			wtforms.validators.InputRequired(),
			wtforms.validators.Length(max=64)
		]
	)
	
	email = wtforms.EmailField(
		validators=[wtforms.validators.Length(max=128)]
	)
	comment = wtforms.TextAreaField(
		validators=[wtforms.validators.Length(max=65536)]
	)


@petition_blueprint.route("/", methods=["GET", "POST"])
@flask.current_app.limiter.limit(
	"2/day",
	exempt_when=lambda: flask.request.method != "POST"
)
def view():
	form = SignatureForm()

	added = False

	if form.validate_on_submit():
		captcha_verification_response = requests.post(
			flask.current_app.config["FRIENDLY_CAPTCHA_ENDPOINT_VERIFICATION"],
			json={
				"solution": form.captcha_solution.data,
				"secret": flask.current_app.config["FRIENDLY_CAPTCHA_SECRET"],
				"sitekey": flask.current_app.config["FRIENDLY_CAPTCHA_SITE_KEY"]
			}
		)

		if not captcha_verification_response.ok:
			flask.current_app.logger.error(
				"Friendly captcha returned status code %s, skipping verification",
				captcha_verification_response.status_code
			)
		elif not captcha_verification_response.json()["success"]:
			raise werkzeug.exceptions.Forbidden

		signature = database.Signature(
			name=form.name.data,
			surname=form.surname.data,
			street=form.street.data,
			city=form.city.data,
			email=form.email.data,
			comment=form.comment.data
		)

		flask.g.sa_session.add(signature)
		flask.g.sa_session.commit()

		added = True

	signature_count = flask.g.sa_session.execute(
		sqlalchemy.select(sqlalchemy.func.count(database.Signature.id))
	).scalars().one()

	return flask.render_template(
		"petition.html",
		form=form,
		added=added,
		signature_count=signature_count
	)


@petition_blueprint.route("/signatures", methods=["GET"])
@utils.login_required
def view_signatures():
	return flask.render_template(
		"signatures.html",
		signatures=flask.g.sa_session.execute(
			sqlalchemy.select(database.Signature)
		).scalars().all()
	)


@petition_blueprint.route("/gdpr", methods=["GET"])
def view_gdpr():
	return flask.render_template("gdpr.html")
