import json
import logging
import os
import typing

import flask
import flask_limiter
import flask_wtf.csrf
import sqlalchemy
import sqlalchemy.orm


logging.basicConfig(
	format="[%(asctime)s] %(levelname)s in %(module)s: %(message)s",
	level=getattr(
		logging,
		os.environ.get(
			"LOGGING_LEVEL",
			"DEBUG"
		)
	)
)


def create_app() -> flask.Flask:
	"""Creates a :class:`Flask <flask.Flask>` app with basic settings already
	applied.

	Loads the config from the file located where the ``CONFIG_LOCATION``
	environment variable describes, or ``$current_working_directory/config.json``
	if it's not set.

	Registers all blueprints in :mod:`.views`.
	"""

	app = flask.Flask(__name__)

	with app.app_context():
		app.logger.info("Setting up app")

		app.logger.debug("Loading environment file")

		for variable in (
			"SECRET_KEY",

			"SIGNATURE_VIEW_USERNAME",
			"SIGNATURE_VIEW_PASSWORD",

			"FRIENDLY_CAPTCHA_SECRET",
			"FRIENDLY_CAPTCHA_SITE_KEY",
			"FRIENDLY_CAPTCHA_ENDPOINT_PUZZLE",
			"FRIENDLY_CAPTCHA_ENDPOINT_VERIFICATION"
		):
			flask.current_app.config[variable] = os.environ[variable]

		app.logger.debug("Creating engine")

		database_url = os.environ["DATABASE_URL"]

		if database_url.startswith("postgres://"):
			database_url = database_url.replace("postgres://", "postgresql://")

		sa_engine = sqlalchemy.create_engine(database_url)

		app.sa_session_class = sqlalchemy.orm.scoped_session(
			sqlalchemy.orm.sessionmaker(
				bind=sa_engine
			)
		)

		app.logger.debug("Setting up extensions")

		app.limiter = flask_limiter.Limiter(
			app,
			key_func=lambda: (
				flask.request.headers["X-Forwarded-For"]
				if "X-Forwarded-For" in flask.request.headers
				else flask.request.remote_addr
			),
			default_limits=["300 per day", "100 per hour"]
		)
		flask_wtf.csrf.CSRFProtect(app)

		@app.before_request
		def before_request() -> None:
			"""TODO: doc"""

			flask.g.sa_session = flask.current_app.sa_session_class()

		@app.teardown_request
		def teardown_request(
			exception: typing.Union[None, Exception]
		) -> None:
			"""Attempts to commit :attr:`flask.g.sa_session` and rolls it back if
			any exception is raised during the process. The exception is then
			logged.

			:param exception: The exception that occurred in the prior request,
				if there was any.
			"""

			if "sa_session" in flask.g:
				# "Clean" the session
				try:
					flask.g.sa_session.commit()
				except Exception as commit_exception:
					flask.g.sa_session.rollback()

					flask.current_app.logger.error(
						"Exception %s raised during the request teardown session commit: %s",
						commit_exception.__class__.__name__,
						(
							commit_exception
							if hasattr(commit_exception, "__str__")
							else "no details"
						)
					)

				flask.current_app.sa_session_class.remove()

		from .views import petition_blueprint

		for blueprint in (petition_blueprint,):
			app.logger.debug(
				"Registering blueprint: %s",
				blueprint
			)

			app.register_blueprint(blueprint)

		@app.cli.command("reflect")
		def reflect() -> None:
			"""Reflects database models."""

			from .database import Base

			with app.sa_session_class() as sa_session:
				Base.metadata.create_all(bind=sa_engine)

		return app
